<?php
namespace  crazy\controllers;
use crazy\models\User;
use crazy\views\UserView;
use crazy\views\HomeView;

class UserController
{
	public function affichageInscription()
	{
        $v = new UserView();
        $v->render(2);
	}
	public function affichageInscriptionPost()
	{
 		$app = \Slim\Slim::getInstance();
 		$request = $app->request;
        $nom = $request->params("nom");
        $prenom = $request->params("prenom");
        $password = $request->params("password");
        $email = $request->params("email");


        $errors = array();
        $user = new User();

        if(isset($email) && !empty($email)){
            $email = filter_var($email,FILTER_SANITIZE_EMAIL);
            $emailOk  = filter_var($email, FILTER_VALIDATE_EMAIL);
        //si vrai on le nettoye au cas ou
            if (! $emailOk){
                 $_SESSION['slim.flash']['error'] =  "L'utilisateur existe déjà";
            }
            $user->email = $email;
        }

        if(isset($password) && !empty($password)){
            $password = filter_var($password,FILTER_SANITIZE_STRING);
            $hash = hash('sha256',$password);
            $salt = uniqid('crazy');
            $user->salt = $salt;
            $user->mdp = $hash.$salt;
        }

        $testExist = User::where('email', $email)
        ->first();

        if (isset($testExist) ) {
            $_SESSION['slim.flash']['error'] =  "L'utilisateur existe déjà";
            $v = new UserView();
            $v->render(2);
        }
        else if(count($errors)!=0){
            $v = new UserView();
            $v->render(2);
        }
        else{
            $user->nom = $nom;
            $user->prenom = $prenom;
            $user->save();
            $_SESSION['slim.flash']['success'] =  'L\'utilisateur a bien été inscrit';
            $v = new HomeView();
            //$v->render();
            $v->render(1);
        }

	}

	public function affichageLogin()
	{
		$v = new UserView();
        $v->render(1);
	}
	public function affichageLoginPost()
	{
		$app = \Slim\Slim::getInstance();
        $request = $app->request;
        $email = $request->params("email");
        $password = $request->params("password");

        $errors = array();


        if(isset($email) && !empty($email)){
            $email = filter_var($email,FILTER_SANITIZE_EMAIL);
            $emailOk  = filter_var($email, FILTER_VALIDATE_EMAIL);
        //si vrai on le nettoye au cas ou
            if (! $emailOk){
              
                $_SESSION['slim.flash']['errors'] = "L'email est incorrect";
            }
        }

        if(isset($password) && !empty($password)){
            $password = filter_var($password,FILTER_SANITIZE_STRING);
        }

        $hash = hash('sha256',$password);

        $user = User::where('email', $email)
        // ->where('mdp', $hash)
        ->first();


        if (isset($user) ) {
            if ($user['mdp'] == ($hash.$user['salt'])) {
                $_SESSION['user'] = $user;
                $_SESSION['slim.flash']['success'] = 'Vous êtes bien connecté';
              
            }
            else{
                $_SESSION['slim.flash']['error'] = "Le password est incorrect";
            }
        }
        else if(count($errors)!=0){

        }
        else{
            $_SESSION['slim.flash']['error']  = "L'utilisateur n'existe pas ";
        }
        $v = new HomeView();
        //$v->render();
        $v->render(1);
          
	}
}