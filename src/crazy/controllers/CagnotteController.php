<?php

namespace  crazy\controllers;
use crazy\models\Cagnotte;
use crazy\views\CagnotteView;

class CagnotteController
{
	/**
    public function creercagnotte($prix) {
        $cagnotte = new Cagnotte();
        $cagnotte->prix = $prix;
        $cagnotte->recolte = 0;
        $id = md5(uniqid(rand(), true));
        $cagnotte->id_cagnotte = $id;
        $cagnotte->url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/'.'cagnotte/'.$id;
        $cagnotte->save();
    }
	*/
    
    public function ajouterArgent($id){
		
        $cagnotte =  Cagnotte::where('url','=',$id)->first();
        $app = \Slim\Slim::getInstance();
        $ajout = $app->request->params('argent');
        $cagnotte->recolte +=$ajout;
        $cagnotte->save();
		
        $this->affichageCagnotte($id);
    }
    
    public function affichageCagnotte($id){
		$cagnotte = Cagnotte::find($id);
        $pourcentageargentcollecte = 
            $this->pourcentageArgentCollecter($id);
                $vue = new CagnotteView();

        if($this->complete($id)){
            $vue->render(-1,$id);
        }else{
        $vue->render($pourcentageargentcollecte,$id);
        }
    }
    
    public function pourcentageArgentCollecter($id){
        $cagnotte = Cagnotte::where('url','=',$id)->first();

        $recolte = $cagnotte->recolte;
        $prix = $cagnotte->prix;
        if($recolte > $prix){
            $recolte = $prix;
        }
        return ($recolte / $prix)*100;
    }
    
    public function complete($id){
        $cagnotte =  Cagnotte::where('url','=',$id)->first();
        if($cagnotte->recolte >= $cagnotte->prix){
            return true;
        }else{
            return false;
        }
    }
}