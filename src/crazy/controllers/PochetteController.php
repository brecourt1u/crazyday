<?php
namespace crazy\controllers;
use crazy\views\PochetteView;
use crazy\models\Pochette;
use crazy\models\Prestation;
use crazy\controller\CagnotteController;
use crazy\models\Cagnotte;
use crazy\models\Contenupochette;
use Slim\Slim;

class PochetteController{
	
	function creerPochette($dest, $message){
		if (!isset($_SESSION['pochette'])){
			$_SESSION['pochette']=array();
			$_SESSION['pochette']['destinataire'] = $dest;
			$_SESSION['pochette']['message'] = $message;
			$_SESSION['pochette']['nom'] = array();
			$_SESSION['pochette']['prix'] = array();
			$_SESSION['pochette']['verrou'] = false;
		}
		return true;
	}
	
	function supprimerpochette(){
		$_SESSION['pochette'] = null;
		session_destroy();
	}
	
	function estVerrouille(){
		return $_SESSION['pochette']['verrou'];
	}
	
	function ajouterpochette($str){
		$app = \Slim\Slim::getInstance();
		$list = Prestation::where( 'nom', 'like', "$str")->get();
		foreach($list as $v){
			$obj = $v;
		}
		if ($_SESSION['pochette']['verrou'] != true ){
				array_push($_SESSION['pochette']['nom'],$obj->nom);
				array_push($_SESSION['pochette']['prix'],$obj->prix);
		}
		$_SESSION['slim.flash']['success'] = 'Votre prestation a bien été ajoutée';

	}
	
	function supprimerObjet($obj){
		if (isset($_SESSION['pochette'][$obj->nom])){
				$pos = array_search($_SESSION['pochette'][$obj->nom]);
				$_SESSION['pochette'][$obj->nom][$pos] =  null;
		}
	}
	
	function validerpochette(){
		


		$_SESSION['pochette']['verrou'] = true;
		$p = new Pochette();
		$prix = $this->calculerPrix();
		// Creation cagnotte
		$cagnotte = new Cagnotte();
        $cagnotte->prix = $prix;
        $cagnotte->recolte = 0;
        $id = md5(uniqid(rand(), true));
        $cagnotte->id_cagnotte = $id;
        // $cagnotte->url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/'.'cagnotte/'.$id;
        $cagnotte->url = $id;
		$cagnotte->save();
		$p->montant = $prix;
		$p->id_cagnotte = $id;
		$p->save();
		
		
		foreach($_SESSION['pochette']['nom'] as $value){
			$list = Prestation::where( 'nom', 'like', "$value")->get();
			$contenu = new Contenupochette();
			$contenu->id_pochette = $p->id;
			foreach($list as $v){
				$contenu->id_prestation = $v->id;
			}
			$contenu->save();
		}
		//var_dump($cagnotte->url);
		$app = Slim::getInstance();
		$url = $app->urlFor('cagnotte', array('id' => $id));
		$app->redirect("$url");
			
		

	}
	
	function calculerPrix(){
		$prix = 0; 
		for($i=0 ; $i < count($_SESSION['pochette']['nom']) ; $i++){
			$prix = $_SESSION['pochette']['prix'][$i] + $prix;
		}
		return $prix;
	}
	
	function affichageCreation(){
		$pn = new pochetteView();
		$pn->render(2);
	}

	function affichagePochette(){
		$pn = new pochetteView();
		$pn->render(1);
	}
}