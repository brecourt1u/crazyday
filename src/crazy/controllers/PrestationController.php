<?php

/**
 * Created by PhpStorm.
 * User: winger2u
 * Date: 11/02/2016
 * Time: 10:55
 */

namespace  crazy\controllers;
use crazy\models\Prestation;
use crazy\views\PrestationView;

class PrestationController
{
    public function listerPrestation($tri = null)
    {
        $lp = null;
        if (!is_null($tri)) {
            switch ($tri) {
                case 'd':
                    $lp = Prestation::orderBy('prix', 'DESC')->get();
                    break;
                case 'c':
                    $lp = Prestation::orderBy('prix', 'ASC')->get();
                    break;
            }
        } else $lp = Prestation::all();
        $v = new PrestationView($lp, PrestationView::TYPE_NORMAL);
        $v->render(1);
    }
    
    public function listerPrestationId($id = null, $tri = null) {
        $p = null;
        if(!is_null($id)) {
            if (!is_null($tri)) {
                switch ($tri) {
                    case 'd':
                        $p = Prestation::where('id', '=', $id)->orderBy('prix', 'DESC')->firstOrFail();
                        break;
                    case 'c':
                        $p = Prestation::where('id', '=', $id)->orderBy('prix', 'ASC')->firstOrFail();
                        break;
                }
            } else $p = Prestation::where('id', '=', $id)->firstOrFail();
            $v = new PrestationView($p, 0);
            $v->render(2);
        }
    }

    public function listerPrestationType($type, $tri = null) {
        $lp = null;
        if (!is_null($tri)) {
            switch ($tri) {
                case 'd':
                    $lp = Prestation::where('type', '=', $type)->orderBy('prix', 'DESC')->get();
                    break;
                case 'c':
                    $lp = Prestation::where('type', '=', $type)->orderBy('prix', 'ASC')->get();
                    break;
            }
        } else $lp = Prestation::where('type', '=', $type)->get();
        $v = new PrestationView($lp, PrestationView::TYPE_TYPE);
        $v->render(1);
    }

    public function addPrestation($request){

        $nom = $request->params("nom");
        $prix = $request->params("prix");
        $pic = $request->params("pic");
        $type = $request->params("type");
        $presta = new Prestation();
        $presta->nom = htmlentities($nom);
        $presta->prix = htmlentities($prix);
        $presta->type = htmlentities($type);
        $file = $_FILES['pic'];
        define ('SITE_ROOT', realpath(dirname(__FILE__)));
        if (isset($_FILES['pic'])) {
            $name = $_FILES['pic']['name'];
            $path =SITE_ROOT."\..\..\..\\img\\$name";
            move_uploaded_file($_FILES['pic']['tmp_name'], $path);
            $presta->img = $name;
        }
        $presta->save();
        $this->listerPrestationId($presta->id);
    }
    
    public function showAddPrestation(){
        $v = new PrestationView(array());
        $v->render(3);
    }
    
}