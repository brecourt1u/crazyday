<?php
namespace crazy\controllers;
use crazy\views\PochetteView;
use crazy\models\Pochette;
use crazy\models\Cagnotte;
use crazy\models\Prestation;
use crazy\views\EnveloppeView;
use crazy\models\Contenupochette;

class EnveloppeController{
	
    public function creationEnveloppe($id){
        $cagnotte =  Cagnotte::where('url','=',$id)->first();
        // var_dump($cagnotte);
        $pochette = Pochette::where('id_cagnotte','=', $cagnotte->id_cagnotte)->first();
        $contenu = Contenupochette::where('id_pochette','=',$pochette->id_pochette)->get();
        $list = null;
        foreach($contenu as $value){
            $list[] = Prestation::find($value->id_prestation);   
        }
        $vue = new EnveloppeView($list,$id);
            
        // $vue = new EnveloppeView($pochette->prestations->toArray(),$id);
		$vue->render();

    }
	
}