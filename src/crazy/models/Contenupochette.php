<?php
/**
 * Created by PhpStorm.
 * User: winger2u
 * Date: 11/02/2016
 * Time: 10:51
 */

namespace crazy\models;


use Illuminate\Database\Eloquent\Model;

class Contenupochette extends Model
{
    protected  $table = 'contenupochette';
    protected  $primaryKey = 'id_pochette';
    public $timestamps = false;

    public function type() {
        return $this->belongsTo('\models\Type', 'type');
    }
}
