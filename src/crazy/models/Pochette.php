<?php
namespace crazy\models;


use Illuminate\Database\Eloquent\Model;

class Pochette extends Model
{
    protected  $table = 'pochette';
    protected  $idColumn = 'id_pochette';
    public $timestamps = false;

    public function type() {
        return $this->belongsTo('\models\Type', 'type');
    }
	
	public function prestations(){
		return $this->belongsToMany("crazy\models\Prestation", "contenupochette", 'id_pochette', 'id_prestation');
	}
}
