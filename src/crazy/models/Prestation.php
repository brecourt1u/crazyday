<?php
/**
 * Created by PhpStorm.
 * User: winger2u
 * Date: 11/02/2016
 * Time: 10:51
 */

namespace crazy\models;


use Illuminate\Database\Eloquent\Model;

class Prestation extends Model
{
    protected  $table = 'prestation';
    protected  $primaryKey = 'id';
    public $timestamps = false;

    public function type() {
        return $this->belongsTo('\models\Type', 'type');
    }
}
