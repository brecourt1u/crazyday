<?php
/**
 * Created by PhpStorm.
 * User: winger2u
 * Date: 11/02/2016
 * Time: 10:51
 */

namespace crazy\models;


use Illuminate\Database\Eloquent\Model;

class Cagnotte extends Model
{
    protected  $table = 'cagnotte';
    protected  $primaryKey = 'id_cagnotte';
    public $timestamps = false;
}
