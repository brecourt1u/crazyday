<?php

/**
 * Created by PhpStorm.
 * User: winger2u
 * Date: 11/02/2016
 * Time: 11:03
 */
namespace crazy\views;
use crazy\views\AbstractView;
use Slim\Slim;
use crazy\models\Type;

class PrestationView extends AbstractView{

    const ALL = 1;
    const ID = 2;
    const TYPE_NORMAL = 21;
    const TYPE_TYPE = 22;

    private $array, $slim, $type;
    const SHOW_ADD = 3; 
    const ADD = 4;




    public function __construct($t = null, $type = null)    {
        $this->slim = Slim::getInstance();
        if (!is_null($t)) {
            $this->array = $t;
        }
        $this->type = $type;
    }

    
    public function render($selecteur = null) {
        $base = $this->slim->request->getRootUri();
        $content = "";
        $url = $this->slim->request->getRootUri() . $this->slim->request->getResourceUri();
        if (!is_null($selecteur)) {
            switch ($selecteur) {
                case self::ALL :
                    $content = $this->htmlAll();
                    break;
                case self::ID :
                    $content = $this->htmlId();
                    break;
                case self::SHOW_ADD :
                    $content = $this->htmlShowAdd();
                    break;
                case self::ADD :
                    $content = $this->htmlShowAdd();
                    break;
            }
            $header = parent::headHTML();
            $footer = parent::footerHTML();
            $footer .=<<<END
            <script type="text/javascript">
$(document).ready(function() {

    $("ul.notes-echelle").addClass("js");
    $("ul.notes-echelle li").addClass("note-off");

    $("ul.notes-echelle input")
        .focus(function() {
            $(this).parents("ul.notes-echelle").find("li").removeClass("note-focus");
            $(this).parent("li").addClass("note-focus");
            $(this).parent("li").nextAll("li").addClass("note-off");
            $(this).parent("li").prevAll("li").removeClass("note-off");
            $(this).parent("li").removeClass("note-off");
        })
        .blur(function() {
            $(this).parents("ul.notes-echelle").find("li").removeClass("note-focus");
            if($(this).parents("ul.notes-echelle").find("li input:checked").length == 0) {
                $(this).parents("ul.notes-echelle").find("li").addClass("note-off");
            }
        })
        .click(function() {
            $(this).parents("ul.notes-echelle").find("li").removeClass("note-checked");
            $(this).parent("li").addClass("note-checked");
        });

    $("ul.notes-echelle li").mouseover(function() {
        $(this).nextAll("li").addClass("note-off");
        $(this).prevAll("li").removeClass("note-off");
        $(this).removeClass("note-off");
    });

    $("ul.notes-echelle").mouseout(function() {
        $(this).children("li").addClass("note-off");
        $(this).find("li input:checked").parent("li").trigger("mouseover");
    });

    $("ul.notes-echelle input:checked").parent("li").trigger("mouseover");
    $("ul.notes-echelle input:checked").trigger("click");

});
</script>
END;
            $types = $this->types();
            $nav = parent::navHTML();
            $urlAction = "";
            switch($this->type) {
                case self::TYPE_NORMAL :
                    $urlAction = $base."/prestations/tri";
                    break;
                case self::TYPE_TYPE :
                    $urlAction = $base."/prestations/type/".$this->typeActuel()."/tri";
                    break;
                default:
                    $urlAction = $base . $this->slim->request->getResourceUri();
            }
            $html = <<<END
$header
$nav
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <p class="lead">Types</p>
                <div class="list-group">
                    $types
                </div>
                <form id="ordrePrix" method="get" action="$urlAction">
                    <input type="radio" name="tri" value="c"/>prix croissant<br />
                    <input type="radio" name="tri" value="d"/>prix decroissant<br />
                    <button type="submit" name="trier">Trier</button>
                </form>
            </div>
<div class="col-md-9">
   <div class="row">
        $content
            </div>
        </div>
 </div>
</div>
$footer
END;
            echo $html;
        }
    }

    public function htmlAll() {
        $base = $this->slim->request->getRootUri();
        $html = "";
		$ajouter = false;
		if(isset($_SESSION['pochette'])){
			$ajouter = true;
		}
        foreach($this->array as $val) {
            $id = $val->id;
            $url = $this->slim->urlFor('prestationId', array('id' => $id));
            $img = $val->img;
            $prix = $val->prix;
            $nom = $val->nom;
            $base = $this->slim->request->getRootUri();
			$button = "";
			if($ajouter){
				$button = 
				"<form class=\"bouton_ajout\" method=\"post\" action=\"$base/ajout\">
				<button type=\"submit\" id=\"ajout\" name=\"$nom\" class=\"btn btn-primary\" >Ajouter</button></form>";
			}
            $html.= <<<END
<div class="col-sm-4 col-lg-4 col-md-4">

                        <div class="thumbnail">
                            <img class="prestationImg" src="$base/../img/$img" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$prix &euro;</h4>
                                <h4><a href="$url">$nom</a></h4>
								$button

                                 <ul class="notes-echelle">
                                    <li>
                                        <label for="note01" title="Note&nbsp;: 1 sur 5">1</label>
                                        <input type="radio" name="notesA" id="note01" value="1" />
                                    </li>
                                    <li>
                                        <label for="note02" title="Note&nbsp;: 2 sur 5">2</label>
                                        <input type="radio" name="notesA" id="note02" value="2" />
                                    </li>
                                    <li>
                                        <label for="note03" title="Note&nbsp;: 3 sur 5">3</label>
                                        <input type="radio" name="notesA" id="note03" value="3" />
                                    </li>
                                    <li>
                                        <label for="note04" title="Note&nbsp;: 4 sur 5">4</label>
                                        <input type="radio" name="notesA" id="note04" value="4" />
                                    </li>
                                    <li>
                                        <label for="note05" title="Note&nbsp;: 5 sur 5">5</label>
                                        <input type="radio" name="notesA" id="note05" value="5" />
                                    </li>
                                </ul>
                                <a href='' class="btn btn-primary btn-sm"> 
                                noter</a>
                            </div>
                        </div>
                    </div>
END;
        }
        return $html;
    }

    public function htmlId() {
        $base = $this->slim->request->getRootUri();
        $html = "";
        $val = $this->array['attributes'];
        $img = $val['img'];
        $prix = $val['prix'];
        $nom = $val['nom'];
        $description = $val['descr'];
            $html.= <<<END
            <div class="col-md-9">
                <div class="thumbnail2">
                    <img class="img-responsive" src="$base/../img/$img" alt="">
                    <div class="caption-full">
                        <h4 class="pull-right">$prix &euro;</h4>
                        <h4><a href="#">$nom</a>
                        </h4>
                        <p>$description</p>
                    </div>
               </div>
            </div>
END;
        return $html;
    }

    public function types() {
        $html="";
        $lt = Type::all();
        foreach($lt as $val) {
            $nom = $val->nom;
            $id = $val->id;
            $url = $this->slim->urlFor('prestationType', array('type' => $id));
            $html.=<<<END
<a href="$url" class="list-group-item">$nom</a>
END;
        }
        return $html;
    }

    private function estTriee() {
        $ressources = explode('/', $this->slim->request->getResourceUri());
        if(in_array(array('d', 'c'), $ressources))
            return true;
        else return false;
    }

    public function htmlShowAdd()
    {
                $app = \Slim\Slim::getInstance();

                $base = $app->request->getRootUri();
                $select ="";
        foreach (Type::all() as $type) {
            $select .= '<option value="'.$type['id'].'">'.$type['nom'].'</option>';
        }
        $html =<<<END
<form action="$base/prestation/add" method="POST" enctype="multipart/form-data">     
<div class="input-group">
  <span class="input-group-addon" id="basic-addon1">Nom</span>
  <input type="text" name="nom" class="form-control" placeholder="Nom" aria-describedby="basic-addon1">
</div>
<div class="input-group">
  <span class="input-group-addon" id="basic-addon1">description</span>
  <input type="text" name="descr" class="form-control" placeholder="Description" aria-describedby="basic-addon1">
</div>
                                <select class="form-control" name="type">
                                    $select
                                </select>
<div class="input-group">
  <span class="input-group-addon" id="basic-addon1">prix</span>
  <input type="text" name="prix" class="form-control" placeholder="Prix" aria-describedby="basic-addon1">
</div>
<div class="input-group">
  <span class="input-group-addon" id="basic-addon1">image</span>
    <input type="file" name="pic" accept="image/*">
</div>
<input type="submit" class="form-control" >
</form>
END;
return $html;
    }

    private function typeActuel() {
        return $this->array[0]->type;
    }
}