<?php
namespace crazy\views;
use crazy\views\AbstractView;
use Slim\Http\Util;

class HomeView extends AbstractView{

    function render($a)    {
        $HTML = "";
        switch ($a) {
            case 1 :
                $HTML = $this->afficherAcceuil();
                break;
        }
        echo $HTML;
    }

    /*
     * Fonction qui affiche la barre de navigation sur chaque page de l'application
     */
    public static function afficherAcceuil()    {
        $res =  parent::headHTML();
        $res .= parent::navHTML();
        $app = \Slim\Slim::getInstance();
        $base = $app->request->getRootUri();
        $res .= <<<END
        <div class="container-fluid demo-3 home-fluid">
            <div id="large-header" class="large-header">
                <canvas id="demo-canvas"></canvas>
                <h1 class="main-title">Mystery Charly Bag</span></h1>

            <header center class="jumbotron">
                <h1>Mistery Charly Bag </h1>

                <p>
                <a class="btn btn-primary btn-large">Créer une pochette mystère</a>
                </p>
            </header>
            </div>


            <div class="home">
                <h2>Bienvenue sur ce site de pochette surprise</h2>
                <p>Mistery Charly Bag est un concept cadeau original. Il s’agit en effet de pochettes surprises
                contenant différents niveaux de prestations en terme d’activités, de restauration, d’hébergement et de présents. 
                La  vision de l’entreprise est d’entraîner le bénéficiaire de la pochette surprise dans une aventure 
                mystérieuse, ponctuée d’activités locales (région Nancy), de clins d’œil ludiques et personnalisés. Les aspirations
                sont de réveiller les plaisirs d’offrir et de recevoir, d’attiser la curiosité et l’enthousiasme pour des choses simples
                parfois  insoupçonnées, de mettre l’humain au centre d’un tourisme ludique et sur mesure et de soutenir le dynamisme des initiatives et commerces 
                nancéens. La joie, le divertissement, l’originalité et la personnalisation sont donc mis à l’honneur. 
                </p>
                <a href="$base/prestation/add" class="btn btn-primary">Créer une pochette surprise</a>

                <div class="row">
                    <div class="col-lg-12">
                        <h3>Retrouvez les meilleures préstations triées par types</h3>
                    </div>
                </div>
                <!-- /.row -->

                <!-- Page Features -->
                <div class="row text-center">

                    <div class="col-md-3 col-sm-6 hero-feature">
                        <div class="thumbnail">
                            <img src="http://placehold.it/800x500" alt="">
                            <div class="caption">
                                <h3>Restauration</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            
                                    <a href="#" class="btn btn-primary">Voir la prestation</a>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 hero-feature">
                        <div class="thumbnail">
                            <img src="http://placehold.it/800x500" alt="">
                            <div class="caption">
                                <h3>Activité</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                
                                    <a href="#" class="btn btn-primary">Voir la prestation</a>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 hero-feature">
                        <div class="thumbnail">
                            <img src="http://placehold.it/800x500" alt="">
                            <div class="caption">
                                <h3>Attention</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                
                                    <a href="#" class="btn btn-primary">Voir la prestation</a>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 hero-feature">
                        <div class="thumbnail">
                            <img src="http://placehold.it/800x500" alt="">
                            <div class="caption">
                                <h3>Hébergement</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                 <a href="#" class="btn btn-primary">Voir la prestation</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
END;
        $res.=parent::footerHTML();
        return $res;
    }
}
