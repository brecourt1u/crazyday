<?php
namespace crazy\views;
use crazy\views\AbstractView;
use Slim\Http\Util;
use Slim\Slim;

class EnveloppeView extends AbstractView{

  private $slim,$list;
  public function __construct($list,$id)
  {
    $this->list =$list;
    $this->slim = Slim::getInstance()->urlFor('enveloppe',array('id'=>$id));
  }

  function render()    {
    $HTML = $this->afficherPrestation();
    echo $HTML;
  }

    /*
     * Fonction qui affiche la barre de navigation sur chaque page de l'application
     */
    public function afficherPrestation()    {

      $header = parent::headHTML();
      $footer = parent::footerHTML();
      $nav = parent::navHTML();
      $content = $this->affichagecontenu();




      
      $html = <<<END
      $header
      $nav
      <div class="container">
        $content
      </div>
    </body>
    $footer
END;
    return $html;
  }

  public function affichagecontenu(){
    $app = \Slim\Slim::getInstance();

    $base = $app->request->getRootUri();

    $content = <<<END
    <h1>Les prestations pour votre pochette mystère</h1>
    <br>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
END;
        $taille = count($this->list);
        for ($i = 1; $i <= $taille; $i++) {
          $content.="<li data-target='#myCarousel' data-slide-to=$i></li>";
        }
        $content.="</ol>
        <!-- Wrapper for slides -->
        <div class='carousel-inner' role='listbox'>";
          foreach($this->list as $value){
            if ($value === $this->list[0]) {
              # code...
            $active = 'active';
            }
            else{
              $active='';
            }
            $content.="<div class='item $active'>
            <img src=\"".$base.'/../img/'.$value->img."\" alt=\"".$value->nom."\" >
            <div class='carousel-caption'>
              <h3>$value->nom</h3>
              <p>$value->descr.'.'</p>
            </div>
          </div>";
        }
        $content.="</div>

        <!-- Left and right controls -->
        <a class='left carousel-control' href='#myCarousel' role='button' data-slide='prev'>
          <span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>
          <span class='sr-only'>Previous</span>
        </a>
        <a class='right carousel-control' href='#myCarousel' role='button' data-slide='next'>
          <span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>
          <span class='sr-only'>Next</span>
        </a>
      </div>"
;
      return $content;
    }

  }