<?php

namespace crazy\views;
use crazy\views\AbstractView;
use Slim\Slim;
use crazy\models\Pochette;
class CagnotteView extends AbstractView
{

    public function render($pageargentcollecte, $id) {
		$slim = Slim::getInstance()->urlFor('cagnotte',array('id'=>$id));
		$enveloppe = Slim::getInstance()->urlFor('enveloppe',array('id'=>$id));
        $header = parent::headHTML();
        $footer = parent::footerHTML();
        $nav = parent::navHTML();
        $content = "";
		$pochette = Pochette::where('id_cagnotte','like', $id); 
        if($pageargentcollecte == -1){
            $content = 
<<<END
<div class="container">
<h1>Félicitations, la cagnotte est complète ! </h1>
<a href="$enveloppe" class="btn btn-primary">Merci</a>
        </div>
END;
		}else{ $content =
<<<END
<div class="container">

            <h1>Participer aux dons </h1>
			<div class="progress">
  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: $pageargentcollecte%;">
    <span class="sr-only">$pageargentcollecte% Complete</span>
  </div>
</div>
            <form name="ajoutargent" method="post" action="$slim">
                Entrez argent à :
                <input type="number" name="argent" />
                <br/>
                <input type="submit" class="btn btn-primary" name="Participer" value="Parcitiper" />
            </form>
			</div>
END;
			} $html =
<<<END
$header $nav <div class="content">
                $content
                </div>
    </body>
    $footer
END;
	echo $html; } }