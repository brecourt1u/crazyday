<?php
namespace crazy\views;
use crazy\controllers\PochetteController;
use crazy\views\AbstractView;
use Slim\Http\Util;


class PochetteView extends AbstractView{

    function render($a)    {
        switch ($a) {
            case 1 :
                $this->afficherPochette();
                break;
            case 2 :
                $this->creationPochette();
                break;
        }
    }

    public function afficherPochette(){
        $res =  parent::headHTML();
        $res .= parent::navHTML();
        $app = \Slim\Slim::getInstance();
        $base = $app->request->getRootUri();
        $p = new PochetteController();
        $msg = "<td>".htmlspecialchars($_SESSION['pochette']['message'])."</td>";
        $dest = "<td>".htmlspecialchars($_SESSION['pochette']['destinataire'])."</td>";
        $res .= <<<END
        <div class="container" id = "corps" >
                    <div class="panel panel-default" >
                        <div class="panel-body" >
                            <legend >Panier</legend >
        <div class="bs-example" style="overflow:hidden" data-example-id="blockquote-reverse">
        <blockquote>
        <p>$msg</p>
        <footer><cite title="Source Title">$dest</cite></footer>
        </blockquote>
        </div>
END;
        echo $res;
        $content = "<table class=\"table table-striped\"><thead>";
        $content .= "<tr><th>Nom</th>";
        $content .= "<th>Prix</th>";
        $content .= "<th>Total</th></tr></thead><tbody>";
        if (isset($_SESSION['pochette'])){
            $nbArticles = count($_SESSION['pochette']['nom']);
            if ($nbArticles <= 0)
                echo '<tr><td>Votre pochette est vide </td></tr>';
            else {
                for ($i = 0; $i < $nbArticles; $i++) {
                    $content .= "<tr><td>".htmlspecialchars($_SESSION['pochette']['nom'][$i])."</td>";
                    $content .= "<td>".htmlspecialchars($_SESSION['pochette']['prix'][$i])."</td>";
                }
                $content .= "<tr><td>".htmlspecialchars("")."</td>";
                $content .= "<td>".htmlspecialchars("")."</td>";
                $content .= "<td>".$p->calculerPrix()."</td></tr>";
                $content .= "</tbody></table>";
                echo $content;

            }
        }
        echo <<<END

        <form class="form-horizontal" action="$base/validerpochette">
            <button id="valide_Pochette" class="btn btn-primary">Valider pochette</button>
        </form>
		<form class="form-horizontal" action="$base/supprimer">
            <button id="supprimer_pochette" class="btn btn-primary">Supprimer pochette</button>
        </form>

END;
    }

    public function creationPochette(){
        $header =  parent::headHTML();
        $nav = parent::navHTML();
        $footer = parent::footerHTML();
        $app = \Slim\Slim::getInstance();
        $base = $app->request->getRootUri();
		$res = "";
        $res .= <<<END
		$header
		$nav
<form class="form-horizontal" method="post" action="$base/creation" enctype="multipart/form-data">
  <div class="form-group">
    <label class="col-sm-2 control-label">Destinataire</label>
    <div class="col-sm-10">
        <input type="text" name="destinataire" class="form-control">
    </div>
    <label class="col-sm-2 control-label">Message</label>
    <div class="col-sm-10">
        <textarea class="form-control" name="message" rows="3"></textarea>
    </div>
	
	<button type="submit" id="creerpochette" name="validPochette" class="btn btn-primary">Commencer !</button>
	
  </div>
</form>
$footer
END;
        echo $res;
    }
}