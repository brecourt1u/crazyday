<?php


namespace crazy\views;

use Slim\Slim;

class AbstractView {
    public static function headHTML() {
        $app = Slim::getInstance();
        $base = $app->request->getRootUri();
        $HTML= <<<END
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Mystery bag </title>
    <link href="$base/../css/bootstrap.min.css" rel="stylesheet">
    <link href="$base/../css/starter-template.css" rel="stylesheet">
    <link href="$base/../css/header.css" rel="stylesheet">
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Crazy charly day" />
    <meta property="og:description" content="Trouver une pochette" />
    <meta property="og:url" content="http://localhost/crazyday/crazyday/index.php/" />
    <meta property="og:site_name" content="Crazy charly day" />
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description" content="Just another colorlib.com site"/>
    <meta name="twitter:title" content="Crazy charly day"/>
</head>
<body>
END;
        return $HTML;
    }

    public static function navHTML() {
        $app = Slim::getInstance();
        $base = $app->request->getRootUri();
		if(!isset($_SESSION['pochette'])){
			$pochette = "<a href=\"$base/pochette\">Creer une pochette</a>";
		}
		else{
			$pochette = "<a href=\"$base/etatpochette\">Voir ma pochette</a>";
		}
        if(isset($_SESSION['user'])){
            $user='<li>
                        <a href="#">'.$_SESSION['user']['prenom'].'</a>
                    </li>'  ;
        }
        else{
            $connexion= "$base/login";
            $inscription= "$base/inscription";
             $user='<li>
                        <a href="'.$connexion.'">Connexion</a>
                        </li>
                        <li>
                        <a href="'.$inscription.'">Inscription</a>
                    </li>';
        }
        $HTML=<<<END
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="$base">Mystery Charly Bag</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">

                    <li>
                        <a href="$base/prestations">Voir les prestations</a>
                    </li>
                    <li>
                        <a href="$base/prestation/add">Ajouter une prestation</a>
                    </li>                  
                    <li>
                        $pochette
                    </li>
                    
                        $user
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

END;
if(isset($_SESSION['slim.flash']['success']) ) {
    $HTML.='<div class="alert alert-success" role="alert">'.$_SESSION['slim.flash']['success'].'</div>';
 $_SESSION['slim.flash']['success'] = null;
 } 
if(isset($_SESSION['slim.flash']['error']) ) {
    $HTML.='<div class="alert alert-error" role="alert">'.$_SESSION['slim.flash']['error'].'</div>';
 $_SESSION['slim.flash']['error'] = null;
 } 
        return $HTML;
    }

    public static function footerHTML() {
        $app = \Slim\Slim::getInstance();
        $base = $app->request->getRootUri();
        $HTML=<<<END
 <div class="container">
        <hr>
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Crazy Charly Day 2016</p>
                </div>
            </div>
        </footer>
    </div>
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="$base/../js/bootstrap.min.js"></script>
    <script src="$base/../js/TweenLite.min.js"></script>
    <script src="$base/../js/EasePack.min.js"></script>
    <script src="$base/../js/rAF.js"></script>
    <script src="$base/../js/header.js"></script>
</body>

</html>
END;
        return $HTML;
    }
}