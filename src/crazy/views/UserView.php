<?php
namespace crazy\views;
use crazy\controllers\PochetteController;
use crazy\views\AbstractView;
use Slim\Http\Util;


class UserView extends AbstractView{

    function render($a)    {
        switch ($a) {
            case 1 :
                $this->afficherLogin();
                break;
            case 2 :
                $this->afficherInscription();
                break;
        }
    }

    public function afficherInscription()
    {

        $res =  parent::headHTML();
        $res .= parent::navHTML();
        $app = \Slim\Slim::getInstance();
        $base = $app->request->getRootUri();
        $url = $app->urlFor('inscription');
        $res .=<<<END

    <div class="container">
        <h1>Inscription</h1>
        <div class="row">
            <div class="col-md-12">         
                <p>Beneficier de toute nos offres en étant inscrit </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"> 
            <?php include 'inc_errors.php'; ?>

            <form method="POST" action="$url">
                <div class="row">

                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label for="nom">Nom</label>
                                <input type="text" name="nom" class="form-control" placeholder="Votre nom" required autofocus>
                            </div>
                            <div class="form-group">
                                <label for="prenom">Prenom</label>
                                <input type="text" name="prenom" class="form-control" placeholder="Votre prenom" required>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="email" class="form-control" placeholder="Votre email" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Votre mot de passe" required>
                            </div>
                            
                        </fieldset>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" value="Inscription" class="btn btn-default"/>
                    </div>
                </div>
            </form>
        </div>
    </div>

END;
echo $res;
    }
    public function afficherLogin()
    {
        $res =  parent::headHTML();
        $res .= parent::navHTML();
        $app = \Slim\Slim::getInstance();
        $base = $app->request->getRootUri();
        $login = $app->urlFor('login');
        $res .= <<<END
  <div class="container" >
      <h1>Login</h1>
    <div class="row">
      <div class="col-md-12">     
        <p>Vous pouvez vous inscrire sur ce site afin de devenir prestataire partenaire</p>
      </div>
    </div>
    

    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title"><strong>Se connecter</strong></h3></div>
        <div class="panel-body">
        <form role="form" method="POST" action="$login ">
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Enter email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-sm btn-default">Se connecter</button>
        </form>
      </div>
    </div>
  </div>

END;
    echo $res;
    }
}