<?php
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use crazy\controllers\HomeController;
use crazy\controllers\PochetteController;
use crazy\controllers\PrestationController;
use crazy\controllers\CagnotteController;

use crazy\controllers\UserController;

use crazy\controllers\EnveloppeController;


session_start();

$config = parse_ini_file('conf/config.ini');

$db = new DB();
$db->addConnection(array (
    'driver' => $config['driver'] ,
    'host' => $config['host'],
    'database' => $config['database'],
    'username' => $config['username'],
    'password' => $config['password'],
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'=>''
));

$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

$app->get('/', function () {
    $c = new HomeController();
    $c->accueil();
});
$app->get('/prestations(/tri)', function() {
    $c = new PrestationController();
    if(!isset($_GET['tri']))
        $c->listerPrestation();
    else {
        $c->listerPrestation($_GET['tri']);
    }
});
$app->map('/prestation/add', function () use($app) {
    $c = new PrestationController();
    $request = $app->request;
    if ($request->isGet()) {
        $c->showAddPrestation();
        return;
    }
    else{
        $c->AddPrestation($request);
        return;        
    }
    
})->via('GET', 'POST')->name('ajouterPrestation');

$app->get('/prestations/:id', function ($id) {
    $c = new PrestationController();
    $c->listerPrestationId($id);
})->name('prestationId');

$app->get('/prestations/type/:type(/tri)', function($type) {
    $c = new PrestationController();
    if(!isset($_GET['tri']))
        $c->listerPrestationType($type);
    else {
        $c->listerPrestationType($type, $_GET['tri']);
    }
})->name('prestationType');

$app->get('/pochette', function () {
    $p = new PochetteController();
    $p->affichageCreation();
});

$app->get('/etatpochette', function () {
    $p = new PochetteController();
	$p->affichagePochette();
});


$app->post('/creation', function () {
    $p = new PochetteController();
	$p->creerPochette($_POST['destinataire'],$_POST['message']);
	$c = new PrestationController();
    $c->listerPrestation();
});

$app->post('/ajout', function () {
	$array = array_keys($_POST);
	$p = new PochetteController();
	
	foreach($array as $value){
		$p->ajouterpochette($value);
	}
	$c = new PrestationController();
    $c->listerPrestation();
});

$app->get('/cagnotte/:id', function($id){
    $c = new CagnotteController();
    $c->affichageCagnotte($id);
})->name('cagnotte');

$app->get('/enveloppe/:id', function($id){
    $c = new EnveloppeController();
    $c->creationEnveloppe($id);
	
})->name('enveloppe');

$app->post('/cagnotte/:id', function($id){
    $c = new CagnotteController();
    $c->ajouterArgent($id);
});


$app->get('/payement', function(){
    print_r('payement � revoir');
});








$app->map('/login', function () use ($app){
    $request = $app->request;
    if ($request->isGet()) {
        $c = new UserController();
        $c->affichageLogin();
        return;
    }
    else{
        $c = new UserController();
        $c->affichageLoginPost();

    }
})->via('GET', 'POST')->name('login');

//route pour l'inscription d'un utilisateur
$app->map('/inscription', function () use ($app){
    $request = $app->request;
    if ($request->isGet()) {
        //afficher($app,array('vue'=>'inscription.php', 'title' => 'inscription'));
        $c = new UserController();
        $c->affichageInscription();
        return;
    }
    else{

        //afficher($app,array('vue'=>'inscription.php', 'title' => 'inscription','errors' => $errors));
        $c = new UserController();
        $c->affichageInscriptionPost();
    }
})->via('GET', 'POST')->name('inscription');

$app->post('/creation', function () {
    $p = new PochetteController();
	$p->creerPochette($_POST['destinataire'],$_POST['message']);
	$c = new PrestationController();
    $c->listerPrestation();
});

$app->get('/validerpochette?', function(){
	$p = new PochetteController();
	$p->validerpochette();
    //$c = new PrestationController();
    //$c->listerPrestation();
});

$app->get('/supprimer', function () {
    $c = new PochetteController();
    $c->supprimerpochette();
	$c = new PrestationController();
    $c->listerPrestation();
	
});

$app->run();	


